// Board.java
package tetris;

/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
*/
public class Board	{
	// Some ivars are stubbed out for you:
	private int width;
	private int height;
	private boolean[][] grid;
	private int[]widths;
	private int[]heights;
	private boolean DEBUG = true;
	boolean committed;
	private int maxHeight;

	private boolean[][] backUp;
	private int[]widthsBack;
	private int[]heightsBack;
	
	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		grid = new boolean[width][height];
		backUp = new boolean[width][height];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				grid[x][y] = false;
				backUp[x][y]=false;
			}
		}
		
		maxHeight=0;
		widths = new int[height];
		heights = new int[width];
		
		widthsBack = new int[height];
		heightsBack = new int[width];
		
		committed = true;
		
	}
	
	
	/**
	 Returns the width of the board in blocks.
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	*/
	public int getMaxHeight() {
		for (int i=0;i<heights.length;i++)
			if(maxHeight<heights[i])
				maxHeight=heights[i];
		return maxHeight;
	}
	
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	*/
	public void sanityCheck() {
		if (DEBUG) {
			// YOUR CODE HERE
		}
	}
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	*/
	public int dropHeight(Piece piece, int x) {
		int h=0, rest=0;
		int[] skirt = piece.getSkirt();
		
		for(int i =0 ; i < skirt.length;i++)
		if (x<0||x+piece.getWidth()>width)
			return -1;
		
		for (int i=0;i<skirt.length;i++) 
			h=getColumnHeight(x+i)-skirt[i];
			if (h>rest)
				rest=h;
		return rest;
		
	}
	
	
	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	*/
	public int getColumnHeight(int x) {
		if (x<0 || x>=width)
			return -1;
		return heights[x]; 
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	*/
	public int getRowWidth(int y) {
		if (y<0 || y>=height)
			return -1;
		return widths[y];
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	*/
	public boolean getGrid(int x, int y) {
		return (x<0	|| y<0 || x>=width || y >=height);
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	*/
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		if (!committed) throw new RuntimeException("place commit problem");
		committed = false;
		
		copiarBackUp();
		int result = PLACE_OK;
		int pX,pY;
		TPoint body[] = piece.getBody();
		for(int i =0; i < body.length;i++){
			pX = x+body[i].x;
			pY = y+body[i].y;

			if(pX<0	||	pY<0 || pX>=width || pY>=height){
				result = PLACE_OUT_BOUNDS;
				break;
			}if(grid[pX][pY]){
				result = PLACE_BAD;
				break;
			}

			grid[pX][pY] = true;

			if(heights[pX]<pY+1)
				heights[pX]=pY+1;

			widths[pY]++;

			if(widths[pY] == width)
				result = PLACE_ROW_FILLED;
		}
		getMaxHeight();
		sanityCheck();
		return result;
	}
	
	private void copiarBackUp(){
		System.arraycopy(widths, 0, widthsBack, 0, widths.length);
		System.arraycopy(heights, 0, heightsBack, 0, heights.length);
		for(int i =0;i<grid.length;i++)
			System.arraycopy(grid[i], 0, backUp[i], 0, grid[i].length);
	}
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	*/
	public int clearRows() {
		int rowsCleared = 0;
		boolean op=true;
		int cont=0;
		for(int i=0;i<getMaxHeight();i++){
			for(int j=1;j<width;j++){
				if(grid[i][j]==op)
					cont++;
				else
					break;
			}
			if(cont==grid[0].length){
				rowsCleared++;
				for(int k=0;k<grid.length;k++)
					grid[i][k]=false;
			}
		}
		sanityCheck();
		return rowsCleared;
	}



	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	*/
	public void undo() {
		// YOUR CODE HERE
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
	public void commit() {
		committed = true;
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
}


