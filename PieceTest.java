package tetris;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.*;

/*
  Unit test for Piece class -- starter shell.
 */
public class PieceTest {
	// You can create data to be used in the your
	// test cases like this. For each run of a test method,
	// a new PieceTest object is created and setUp() is called
	// automatically by JUnit.
	// For example, the code below sets up some
	// pyramid and s pieces in instance variables
	// that can be used in tests.
	private Piece pyr1, pyr2, pyr3, pyr4,l1,l11,l12,l13,l2,l21,l22,l23,s1,s11,s2,s21,sqr,sti,sti2;
	private Piece s, sRotated;

	@Before
	public void setUp() throws Exception {
		
		pyr1 = new Piece(Piece.PYRAMID_STR);
		pyr2 = pyr1.computeNextRotation();
		pyr3 = pyr2.computeNextRotation();
		pyr4 = pyr3.computeNextRotation();
		
		l1 = new Piece(Piece.L1_STR);
		l11 = l1.computeNextRotation();
		l12 = l11.computeNextRotation();
		l13 = l12.computeNextRotation();
		
		l2 = new Piece(Piece.L2_STR);
		l21 = l2.computeNextRotation();
		l22 = l21.computeNextRotation();
		l23 = l22.computeNextRotation();
		
		s1 = new Piece(Piece.S1_STR);
		s11 = s1.computeNextRotation();
		
		s2 = new Piece(Piece.S2_STR);
		s21 = s2.computeNextRotation();
		
		sqr= new Piece(Piece.SQUARE_STR);
		
		sti= new Piece(Piece.STICK_STR);
		sti2 = sti.computeNextRotation();
		
		s = new Piece(Piece.S1_STR);
		sRotated = s.computeNextRotation();
	}
	
	// Here are some sample tests to get you started
	
	@Test
	public void testSampleSize() {
		// Check size of pyr piece
		assertEquals(3, pyr1.getWidth());
		assertEquals(2, pyr1.getHeight());
		assertEquals(2, pyr2.getWidth());
		assertEquals(3, pyr2.getHeight());
		assertEquals(3, pyr3.getWidth());
		assertEquals(2, pyr3.getHeight());
		assertEquals(2, pyr4.getWidth());
		assertEquals(3, pyr4.getHeight());
		
		assertEquals(2, l1.getWidth());
		assertEquals(3, l1.getHeight());
		assertEquals(3, l11.getWidth());
		assertEquals(2, l11.getHeight());
		assertEquals(2, l12.getWidth());
		assertEquals(3, l12.getHeight());
		assertEquals(3, l13.getWidth());
		assertEquals(2, l13.getHeight());

		assertEquals(2, l2.getWidth());
		assertEquals(3, l2.getHeight());
		assertEquals(3, l21.getWidth());
		assertEquals(2, l21.getHeight());
		assertEquals(2, l22.getWidth());
		assertEquals(3, l22.getHeight());
		assertEquals(3, l23.getWidth());
		assertEquals(2, l23.getHeight());

		assertEquals(3, s1.getWidth());
		assertEquals(2, s1.getHeight());
		assertEquals(2, s11.getWidth());
		assertEquals(3, s11.getHeight());

		assertEquals(3, s2.getWidth());
		assertEquals(2, s2.getHeight());
		assertEquals(2, s21.getWidth());
		assertEquals(3, s21.getHeight());

		assertEquals(2, sqr.getWidth());
		assertEquals(2, sqr.getHeight());

		assertEquals(1, sti.getWidth());
		assertEquals(4, sti.getHeight());
		assertEquals(4, sti2.getWidth());
		assertEquals(1, sti2.getHeight());
		
		
		// Now try after rotation
		// Effectively we're testing size and rotation code here
		//assertEquals(2, pyr2.getWidth());
		//assertEquals(3, pyr2.getHeight());
		
		// Now try with some other piece, made a different way
		Piece l = new Piece(Piece.STICK_STR);
		assertEquals(1, l.getWidth());
		assertEquals(4, l.getHeight());
	}
	
	
	// Test the skirt returned by a few pieces
	@Test
	public void testSampleSkirt() {
		// Note must use assertTrue(Arrays.equals(... as plain .equals does not work
		// right for arrays.
		assertTrue(Arrays.equals(new int[] {0, 0, 0}, pyr1.getSkirt()));
		assertTrue(Arrays.equals(new int[] {1, 0}, 	  pyr2.getSkirt()));
		assertTrue(Arrays.equals(new int[] {1, 0, 1}, pyr3.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 1}, 	  pyr4.getSkirt()));

		assertTrue(Arrays.equals(new int[] {0, 0},	  l1.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 0, 0}, l11.getSkirt()));
		assertTrue(Arrays.equals(new int[] {2, 0},	  l12.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 1, 1}, l13.getSkirt()));

		
		assertTrue(Arrays.equals(new int[] {0, 0}, 	  l2.getSkirt()));
		assertTrue(Arrays.equals(new int[] {1, 1, 0}, l21.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 2}, 	  l22.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 0, 0}, l23.getSkirt()));

				
		assertTrue(Arrays.equals(new int[] {0, 0, 1}, s1.getSkirt()));
		assertTrue(Arrays.equals(new int[] {1, 0}, s11.getSkirt()));

		assertTrue(Arrays.equals(new int[] {1, 0, 0}, s2.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 1}, s21.getSkirt()));

		assertTrue(Arrays.equals(new int[] {0, 0}, sqr.getSkirt()));
		
		assertTrue(Arrays.equals(new int[] {0}, sti.getSkirt()));
		assertTrue(Arrays.equals(new int[] {0, 0, 0, 0}, sti2.getSkirt()));

		
		assertTrue(Arrays.equals(new int[] {0, 0, 1}, s.getSkirt()));
		assertTrue(Arrays.equals(new int[] {1, 0}, sRotated.getSkirt()));
	}
	
	
}
